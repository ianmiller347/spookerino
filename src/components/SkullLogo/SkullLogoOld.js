import React from 'react';

const SkullLogoOld = () => (
  <svg
    class="skull-vector"
    xmlns="http://www.w3.org/2000/svg"
    x="0"
    y="0"
    viewBox="0 0 15 15"
  >
    <path
      class="eye eye--left"
      d="M3.504,6.83C3.421,6.857,3.37,6.913,3.373,7.024c0.308,1.938,1.616,3.536,3.842,3.126
  C7.002,8.019,5.745,6.933,3.504,6.83z"
    />
    <path
      class="eye eye--right"
      d="M8.778,10.215c2.196-0.125,3.61-1.379,3.776-3.319C10.321,6.727,8.55,7.923,8.778,10.215z"
    />
    <line class="nose nose-left" x1="7" x2="7" y1="11" y2="12" />
    <line class="nose nose-right" x1="9" x2="9" y1="11" y2="12" />

    <path
      class="skull-2"
      d="M10,
        13.4C11,
        15.5,
        19-1.5,
        8,
        1-0-.3-.31,
        9.32,
        6, 13.65"
    />
    <line class="teeth teeth-1" x1="6" x2="6" y1="13" y2="15" />

    <line class="teeth teeth-2" x1="8" x2="8" y1="13" y2="15" />
    <line class="teeth teeth-3" x1="10" x2="10" y1="13" y2="15" />
    <line class="teeth teeth-bottom" x1="6" x2="10" y1="15" y2="15" />
    <line class="blood-drop" x1="7" x2="7" y1="14" y2="15" />
  </svg>
);

export default SkullLogoOld;
