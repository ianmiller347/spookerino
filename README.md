# Spookerino
An SPA with an animated spooky skull SVG with SVG rain effects.
See it live here: https://spookerino.com


_This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app)._
